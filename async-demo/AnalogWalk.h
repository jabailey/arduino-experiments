#pragma ONCE

#include <VariableTimedAction.h>

class AnalogWalk : public VariableTimedAction {
  public:
    AnalogWalk(int pin = 13)
      : pin(pin),
        value(0),
        increasing(true)
    {}

  private:
    int pin;
    int value;
    bool increasing;
    
    virtual unsigned long run() override {
      if (increasing) {
        increasing = ++value < 255;
      }
      else {
        increasing = --value <= 0;
      }
      analogWrite(pin, value);
      return 0;
    }
};
