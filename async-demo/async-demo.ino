
#include "AnalogWalk.h"
#include "VariableTimedSequence.h"
#include "MorseSequencer.h"

#include <VariableTimedAction.h>

// Define Pins
#define BLUE_PIN 3
#define GREEN_PIN 5
#define RED_PIN 6
#define SOS_PIN 13
#define HELLO_PIN 12

class SosCycle : public VariableTimedAction {
  public:
    virtual unsigned long run() override {
      static MorseSequence* sos;
      if (sos == NULL) {
        sos = MorseSequencer(22, SOS_PIN).s()->o()->s()->wordBreak()->build();
        sos->start(100, true);
      }
      else {
        delete sos;
        sos = NULL;
      }
      return 0;
    }
};

AnalogWalk* redWalk;
AnalogWalk* greenWalk;
AnalogWalk* blueWalk;
MorseSequence* hello;
SosCycle sos_cycle;

void setup()
{
  pinMode(RED_PIN, OUTPUT);
  pinMode(GREEN_PIN, OUTPUT);
  pinMode(BLUE_PIN, OUTPUT);
  pinMode(SOS_PIN, OUTPUT);
  pinMode(HELLO_PIN, OUTPUT);

  redWalk = new AnalogWalk(RED_PIN);
  greenWalk = new AnalogWalk(GREEN_PIN);
  blueWalk = new AnalogWalk(BLUE_PIN);
  redWalk->start(7, true);
  greenWalk->start(11, true);
  blueWalk->start(13, true);

  hello = MorseSequencer(128, HELLO_PIN).h()->e()->l()->l()->o()->comma()->wordBreak()->w()->o()->r()->l()->d()->bang()->wordBreak()->build();
  hello->start(100, true);

  sos_cycle.start(8400, true);
}

// main loop
void loop()
{
  VariableTimedAction::updateActions();
}
