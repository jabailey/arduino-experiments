#ifndef __PLACEMENT_NEW__
#define __PLACEMENT_NEW__

// Overloaded new operator that drops the instance into an existing memory
// location rather than allocating new memory.
inline void *operator new(size_t, void *buf) { return buf; }

#endif
