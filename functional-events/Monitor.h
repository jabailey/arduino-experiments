#ifndef __MONITOR__
#define __MONITOR__

#include "placement_new.h"
#include "Rate.h"
#include "Test.h"

#ifndef MONITOR_CAPACITY
#define MONITOR_CAPACITY 8
#endif

class Monitor {
  public:
    static const unsigned int CAPACITY = MONITOR_CAPACITY;
    
    Monitor(Rate& rate, unsigned long ms) {
      id = rate.every(ms, this, &Monitor::run);
    }

    unsigned long run(Rate* rate, unsigned int id) {
      for (int i=0; i<CAPACITY; ++i) {
        if (_tests[i]._callback && _tests[i]._test()) {
          if (!_tests[i]._callback()) {
            _tests[i]._test.reset();
          }
        }
      }
    }

    template<typename T, typename... Args>
    long when(bool(*callback)(), Args... args) {
      for (int i=0; i<CAPACITY; ++i) {
        if (!_tests[i]._callback) {
          _tests[i]._callback.init(callback);
          new(&_tests[i]._test) T(args...);
          return i;
        }
      }
      return -1;
    }

  private:
    class TestCallback {
      public:
        GenericTest _test;
        Function<bool()> _callback;
    };
    
    unsigned int id;
    TestCallback _tests[CAPACITY];
};

#endif
