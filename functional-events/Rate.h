#ifndef __RATE__
#define __RATE__

#include "Function.h"

#ifndef RATE_CAPACITY
#define RATE_CAPACITY 8
#endif

class Rate {
  private:
    typedef unsigned long(*Callback)(Rate* rate, unsigned int id);
    typedef Function<unsigned long(Rate* rate, unsigned int id)> CallbackFn;
    
  public:
    static const unsigned int CAPACITY = RATE_CAPACITY;
    
    long every(unsigned long ms, Callback fn, bool run_immediately = true) {
      if (ms == 0) {
        return -2;
      }
      for (unsigned int i=0; i<CAPACITY; ++i) {
        if (!_callbacks[i].fn) {
          _callbacks[i].ms = ms;
          _callbacks[i].last_run = 0;
          _callbacks[i].fn.init(fn);
          if (run_immediately) {
            _callbacks[i].fn(this, i);
          }
          return i;
        }
      }
      return -1;
    }

    template<typename T>
    long every(
      unsigned long ms,
      T* ptr,
      unsigned long(T::*fn)(Rate* rate, unsigned int id),
      bool run_immediately = true)
    {
      if (ms == 0) {
        return -2;
      }
      for (unsigned int i=0; i<CAPACITY; ++i) {
        if (!_callbacks[i].fn) {
          _callbacks[i].ms = ms;
          _callbacks[i].last_run = 0;
          _callbacks[i].fn.init(ptr, fn);
          if (run_immediately) {
            _callbacks[i].fn(this, i);
          }
          return i;
        }
      }
      return -1;
    }

    bool stop(unsigned int i) {
      if (i >= CAPACITY || !_callbacks[i].fn) {
        return false;
      }
      _callbacks[i].fn.reset();
      return true;
    }

    void run() {
      unsigned long now = millis();
      for (unsigned int i=0; i<CAPACITY; ++i) {
        if (_callbacks[i].fn && now - _callbacks[i].last_run >= _callbacks[i].ms) {
          _callbacks[i].last_run = now;
          unsigned long result = _callbacks[i].fn(this, i);
          if (result != 0) {
            _callbacks[i].ms = result;
          }
        }
      }
    }

  private:
    class RateCallback {
      public:
        unsigned long ms;
        unsigned long last_run;
        CallbackFn fn;
    };

    RateCallback _callbacks[CAPACITY];
};

#endif
