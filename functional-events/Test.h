#ifndef __TEST__
#define __TEST__

#include "Function.h"

// Abstract base class for a boolean Function, following the same static
// polymorphism pattern found in Function::AbstractInvoker.
class Test : public Function<bool()>
{
  public:
    Test() {}
    
    virtual ~Test() = default;

  protected:
    template<typename T>
    explicit Test(bool(T::*fn)())
      : Function<bool()>(reinterpret_cast<T*>(this), fn)
    {}
};

// Boolean Function that holds the data memory to encapsulate a test on a
// call to digitalRead.
class DigitalReadTest : public Test
{
  public:
    /**
     * @param pin Pin to check with digitalRead
     * @param value Value (HIGH or LOW) to compare the result to
     * @param debounce Number of milliseconds 
     */
    DigitalReadTest(
      unsigned char pin,
      unsigned char value = HIGH,
      unsigned int debounce = 0)
      : Test(&DigitalReadTest::test)
      , _pin(pin)
      , _value(value)
      , _debounce(debounce)
      , _last_ping(0)
    {}

    ~DigitalReadTest() = default;

    bool test() {
      if (millis() - _last_ping < _debounce || digitalRead(_pin) != _value) {
        return false;
      }
      _last_ping = millis();
      return true;
    }

  private:
    unsigned char _pin;
    unsigned char _value;
    unsigned int _debounce;
    unsigned long _last_ping;
};

class GenericTest : public Test
{
  public:
    ~GenericTest() = default;
    
  private:
    char _padding[sizeof(DigitalReadTest) - sizeof(Test)];
};

#endif
