#ifndef __FUNCTION__
#define __FUNCTION__

#include "placement_new.h"

#include <assert.h>

// Base template for convenience; specialized implementation below.
template<typename F>
class Function;

// Loose immitation of std::function.
// Tries to avoid dynamic memory for better performance in an embedded environment.
template<typename R, typename... Args>
class Function<R(Args...)> {
  public:
    // Default constructor yields an uncallable instance.
    Function()
    {
      reset();
    }

    // Constructor for a normal function pointer.
    template<typename F>
    explicit Function(F fn)
    {
      init(fn);
    }

    // Constructor for pointer to an object member function.
    template<typename B, typename F>
    explicit Function(B* ptr, F fn)
    {
      init(ptr, fn);
    }

    // Copy constructor is deleted until I can figure out how to make it work.
    // Using the placement new operator on a GenericInvoker seems like a viable
    // approach to get polymorphic behavior without dynamic allocation, but I
    // don't quite grok how to copy it without mangling it.
    Function<R(Args...)>& operator=(const Function<R(Args...)>& o) = delete;

    // Resets this instance to the default, non-callable state.
    void reset() {
      new(&_invoker) AbstractInvoker;
    }

    // Since the copy constructor is unavailable, this allows an instance to be
    // updated after it was constructed.
    template<typename F>
    void init(F fn) {
      new(&_invoker) FunctionInvoker<F>(fn);
    }
    
    // Since the copy constructor is unavailable, this allows an instance to be
    // updated after it was constructed.
    template<typename B, typename F>
    void init(B* ptr, F fn) {
      new(&_invoker) MemberInvoker<B>(ptr, fn);
    }

    // Bool operator; returns true iff this instance is callable.
    explicit operator bool() const {
      return _invoker;
    }

    // Calls this instance. Behavior is undefined if the instance is not callable.
    R operator()(Args... args) {
      return _invoker(args...);
    }
    
  private:
    // Virtual base class for invoking different types of function pointers.
    class AbstractInvoker {
      public:
        virtual ~AbstractInvoker() = default;
        
        virtual operator bool() const {
          return false;
        }
        
        virtual R operator()(Args...) {}
    };

    // Class for wrapping and invoking a normal function pointer.
    // This logic could easily be built straight into Function, but then
    // Function wouldn't be able to also handle object member functions.
    template<typename F>
    class FunctionInvoker : public AbstractInvoker {
      public:
        friend Function<R(Args...)>;
        
        FunctionInvoker(const F& fn)
          : _fn(fn)
        {
          assert(_fn);
        }
        
        ~FunctionInvoker() = default;
        
        virtual operator bool() const override {
          return true;
        }
        
        virtual R operator()(Args... args) override {
          return _fn(args...);
        }
        
      private:
        F _fn;
    };

    // Class for wrapping and invoking a pointer to a function on an object.
    // This has to be separate from Function in order to avoid making Function
    // templated on the class type for the function pointer.
    template<typename B>
    class MemberInvoker : public AbstractInvoker {
      public:
        typedef R (B::*F)(Args...);
        
        MemberInvoker(B* ptr, const F& fn)
          : _ptr(ptr), _fn(fn)
        {
          assert(_ptr);
          assert(_fn);
        }
        
        ~MemberInvoker() = default;
        
        virtual operator bool() const override {
          return true;
        }
        
        virtual R operator()(Args... args) override {
          return (_ptr->*_fn)(args...);
        }
        
      private:
        B* _ptr;
        F _fn;
    };

    // This is where the non-dynamic allocation gets interesting.
    // Function contains a GenericInvoker value, and it allocates a specialized
    // FunctionInvoker or MemberInvoker into that memory location.
    // The sub-classes are larger than AbstractInvoker, so this class must exist
    // to ensure that Function has enough space to hold either of the specialized
    // classes. The virtual methods must be implemented on this class to call 
    // themselves via the 'this' pointer in order to force late binding to get to
    // the specialized implementations.
    class GenericInvoker : public AbstractInvoker {
      public:
        ~GenericInvoker() = default;
        
        virtual operator bool() const override {
          return !!(*this);
        }
        
        virtual R operator()(Args... args) override {
          return (*this)(args...);
        }
        
      private:
        // Padding to bring the size of this class up to the size of the larger
        // of the two specialized classes.
        int _padding[sizeof(MemberInvoker<AbstractInvoker>) - sizeof(AbstractInvoker)];
    };

    GenericInvoker _invoker;
};

#endif
