#include "Rate.h"
#include "Monitor.h"

class Foo {
  public:
    unsigned long bar(Rate* rate, unsigned int id) {
      if (--iterations_remaining == 0) {
        rate->stop(id);
      }
      Serial.print("Bar. ");
      Serial.print(iterations_remaining);
      Serial.println(" iterations remaining.");
      return 10*iterations_remaining;
    }
    
  private:
    unsigned long iterations_remaining = 100;
};

unsigned long baz(Rate* rate, unsigned int id) {
  Serial.println("Baz.");
  return 0;
}

Rate rate;
Foo foo;
Monitor monitor(rate, 10);

bool button9() {
  Serial.println("Got button 9 press!");
  return true;
}

bool button8() {
  Serial.println("Got button 8 press!");
  return true;
}

void setup() {
  Serial.begin(9600);
  Serial.println("Program begin...");
  delay(1000);
  rate.every(1000, &foo, &Foo::bar);
  rate.every(1000, &baz);
  pinMode(9, INPUT_PULLUP);
  pinMode(8, INPUT_PULLUP);
  monitor.when<DigitalReadTest>(&button9, 9, LOW, 400);
  monitor.when<DigitalReadTest>(&button8, 8, LOW, 400);
}

void loop() {
  rate.run();
}
