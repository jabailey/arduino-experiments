#ifndef __MORSE_SEQUENCER__
#define __MORSE_SEQUENCER__

#include "VariableTimedSequence.h"


class MorseSequence : public VariableTimedSequence {
    friend class MorseSequencer;

  private:
    class Write;
    class Pause;

  public:
    MorseSequence(
      unsigned int capacity,
      unsigned char pin,
      void(*write)(unsigned char, unsigned char) = &digitalWrite,
      unsigned long dot_duration = 100,
      bool repeat = true)
      : VariableTimedSequence(
          (Action * *)malloc(capacity * sizeof(Action*)),
          0,
          true,
          repeat)
    {
      _dot_on = new Write(pin, HIGH, write, dot_duration);
      _dash_on = new Write(pin, HIGH, write, dot_duration * 5);
      _off = new Write(pin, LOW, write, dot_duration);
      _letter_pause = new Pause(dot_duration * 2);
      _word_pause = new Pause(dot_duration * 6);
    }

    ~MorseSequence() {
      delete(_dot_on);
      delete(_dash_on);
      delete(_off);
      delete(_letter_pause);
      delete(_word_pause);
    }

  private:
    Write* _dot_on;
    Write* _dash_on;
    Write* _off;
    Pause* _letter_pause;
    Pause* _word_pause;

    unsigned int& num_actions() {
      return _num_actions;
    }
    
    Action** actions() {
      return _actions;
    }

    class Write : public VariableTimedSequence::Action {
      public:
        Write(
          unsigned char pin,
          unsigned char value,
          void(*write)(unsigned char, unsigned char) = &digitalWrite,
          unsigned long duration = 100)
          : _pin(pin)
          , _value(value)
          , _write(write)
          , _duration(duration)
        {}

        virtual unsigned long run() override {
          _write(_pin, _value);
          return _duration;
        }

      private:
        unsigned char _pin;
        unsigned char _value;
        void(*_write)(unsigned char, unsigned char);
        unsigned long _duration;
    };

    class Pause : public VariableTimedSequence::Action {
      public:
        Pause(
          unsigned long duration = 100)
          : _duration(duration)
        {}

        virtual unsigned long run() override {
          return _duration;
        }

      private:
        unsigned long _duration;
    };
};

class MorseSequencer {
  public:
    MorseSequencer(
      unsigned int capacity,
      unsigned char pin,
      void(*write)(unsigned char, unsigned char) = &digitalWrite,
      unsigned long dot_duration = 100,
      bool repeat = true)
      : _capacity(capacity)
      , _built(false)
    {
      _sequence = new MorseSequence(capacity, pin, write, dot_duration, repeat);
    }

    ~MorseSequencer() {
      if (!_built) {
        delete _sequence;
      }
    }

    MorseSequencer* dot() {
      if (_built || _sequence->num_actions() + 2 > _capacity) {
        return this;
      }
      _sequence->actions()[_sequence->num_actions()++] = _sequence->_dot_on;
      _sequence->actions()[_sequence->num_actions()++] = _sequence->_off;
      return this;
    }

    MorseSequencer* dash() {
      if (_built || _sequence->num_actions() + 2 > _capacity) {
        return this;
      }
      _sequence->actions()[_sequence->num_actions()++] = _sequence->_dash_on;
      _sequence->actions()[_sequence->num_actions()++] = _sequence->_off;
      return this;
    }

    MorseSequencer* letterBreak() {
      if (_built || _sequence->num_actions() + 1 > _capacity) {
        return this;
      }
      _sequence->actions()[_sequence->num_actions()++] = _sequence->_letter_pause;
      return this;
    }

    MorseSequencer* wordBreak() {
      if (_built || _sequence->num_actions() + 1 > _capacity) {
        return this;
      }
      _sequence->actions()[_sequence->num_actions()++] = _sequence->_word_pause;
      return this;
    }

    MorseSequencer* a() {
      return dot()->dash()->letterBreak();
    }
    MorseSequencer* b() {
      return dash()->dot()->dot()->dot()->letterBreak();
    }
    MorseSequencer* c() {
      return dash()->dot()->dash()->dot()->letterBreak();
    }
    MorseSequencer* d() {
      return dash()->dot()->dot()->letterBreak();
    }
    MorseSequencer* e() {
      return dot()->letterBreak();
    }
    MorseSequencer* f() {
      return dot()->dot()->dash()->dot()->letterBreak();
    }
    MorseSequencer* g() {
      return dash()->dash()->dot()->letterBreak();
    }
    MorseSequencer* h() {
      return dot()->dot()->dot()->dot()->letterBreak();
    }
    MorseSequencer* i() {
      return dot()->dot()->letterBreak();
    }
    MorseSequencer* j() {
      return dot()->dash()->dash()->dash()->letterBreak();
    }
    MorseSequencer* k() {
      return dash()->dot()->dash()->letterBreak();
    }
    MorseSequencer* l() {
      return dot()->dash()->dot()->dot()->letterBreak();
    }
    MorseSequencer* m() {
      return dash()->dash()->letterBreak();
    }
    MorseSequencer* n() {
      return dash()->dot()->letterBreak();
    }
    MorseSequencer* o() {
      return dash()->dash()->dash()->letterBreak();
    }
    MorseSequencer* p() {
      return dot()->dash()->dash()->dot()->letterBreak();
    }
    MorseSequencer* q() {
      return dash()->dash()->dot()->dash()->letterBreak();
    }
    MorseSequencer* r() {
      return dot()->dash()->dot()->letterBreak();
    }
    MorseSequencer* s() {
      return dot()->dot()->dot()->letterBreak();
    }
    MorseSequencer* t() {
      return dash()->letterBreak();
    }
    MorseSequencer* u() {
      return dot()->dot()->dash()->letterBreak();
    }
    MorseSequencer* v() {
      return dot()->dot()->dot()->dash()->letterBreak();
    }
    MorseSequencer* w() {
      return dot()->dash()->dash()->letterBreak();
    }
    MorseSequencer* x() {
      return dash()->dot()->dot()->dash()->letterBreak();
    }
    MorseSequencer* y() {
      return dash()->dot()->dash()->dash()->letterBreak();
    }
    MorseSequencer* z() {
      return dash()->dash()->dot()->dot()->letterBreak();
    }

    MorseSequencer* n1() {
      return dot()->dash()->dash()->dash()->dash()->letterBreak();
    }
    MorseSequencer* n2() {
      return dot()->dot()->dash()->dash()->dash()->letterBreak();
    }
    MorseSequencer* n3() {
      return dot()->dot()->dot()->dash()->dash()->letterBreak();
    }
    MorseSequencer* n4() {
      return dot()->dot()->dot()->dot()->dash()->letterBreak();
    }
    MorseSequencer* n5() {
      return dot()->dot()->dot()->dot()->dot()->letterBreak();
    }
    MorseSequencer* n6() {
      return dash()->dot()->dot()->dot()->dot()->letterBreak();
    }
    MorseSequencer* n7() {
      return dash()->dash()->dot()->dot()->dot()->letterBreak();
    }
    MorseSequencer* n8() {
      return dash()->dash()->dash()->dot()->dot()->letterBreak();
    }
    MorseSequencer* n9() {
      return dash()->dash()->dash()->dash()->dot()->letterBreak();
    }
    MorseSequencer* n0() {
      return dash()->dash()->dash()->dash()->dash()->letterBreak();
    }

    MorseSequencer* period() {
      return dot()->dash()->dot()->dash()->dot()->dash()->letterBreak();
    }
    MorseSequencer* comma() {
      return dash()->dash()->dot()->dot()->dash()->dash()->letterBreak();
    }
    MorseSequencer* question() {
      return dot()->dot()->dash()->dash()->dot()->dot()->letterBreak();
    }
    MorseSequencer* apostrophe() {
      return dot()->dash()->dash()->dash()->dash()->dot()->letterBreak();
    }
    MorseSequencer* bang() {
      return dash()->dot()->dash()->dot()->dash()->dash()->letterBreak();
    }

    MorseSequence* build() {
      _built = true;
      return _sequence;
    }

  private:
    unsigned int _capacity;
    bool _built;
    MorseSequence* _sequence;
};

#endif
