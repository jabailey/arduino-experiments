#ifndef __VARIABLE_TIMED_SEQUENCE__
#define __VARIABLE_TIMED_SEQUENCE__

#include <VariableTimedAction.h>

class VariableTimedSequence : public VariableTimedAction {
  public:

    class Action {
      public:
        virtual unsigned long run() = 0;
    };

    VariableTimedSequence(
      Action** actions,
      unsigned int num_actions,
      bool autorelease = true,
      bool repeat = true)
      : _actions(actions)
      , _num_actions(num_actions)
      , _autorelease(autorelease)
      , _repeat(repeat)
      , _next_action(0)
    {}

    ~VariableTimedSequence() {
      stop();
      if (_autorelease) {
        free(_actions);
      }
    }

  protected:
    unsigned int _num_actions;
    Action** _actions;

  private:
    bool _autorelease;
    bool _repeat;
    unsigned int _next_action;

    virtual unsigned long run() override {
      if (_next_action >= _num_actions) {
        stop();
        return 0;
      }
      Action* action = _actions[_next_action];
      auto retval = action->run();
      if (++_next_action >= _num_actions) {
        if (_repeat) {
          _next_action = 0;
        }
        else {
          stop();
        }
      }
      return retval;
    }
};

#endif
