
#include "VariableTimedSequence.h"
#include "MorseSequencer.h"

#include <VariableTimedAction.h>

#define RED_PIN_OUT 13
#define RED_PIN_IN 9
#define GREEN_PIN_OUT 12
#define GREEN_PIN_IN 8

class MorseController : public VariableTimedAction {
  public:
    MorseController(unsigned char in_pin, unsigned char out_pin)
      : _in_pin(in_pin), _out_pin(out_pin)
    {}

    virtual unsigned long run() override {
      if (digitalRead(_in_pin) == LOW && millis() - _last_press > 1000) {
        _last_press = millis();
        Serial.print("Pin ");
        Serial.print(_in_pin);
        Serial.print(" press; mode ");
        Serial.println(_mode);
        if (_sequence != NULL) {
          delete _sequence;
          _sequence = NULL;
        }
        switch (_mode) {
          case 0:
            _sequence = MorseSequencer(22, _out_pin).s()->o()->s()->wordBreak()->build();
            _sequence->start(1, true);
            _mode = 1;
            break;
          case 1:
            _sequence = MorseSequencer(128, _out_pin).h()->e()->l()->l()->o()->comma()->wordBreak()->w()->o()->r()->l()->d()->bang()->wordBreak()->build();
            _sequence->start(1, true);
            _mode = 2;
            break;
          case 2:
            _sequence = MorseSequencer(5, _out_pin).dot()->build();
            _sequence->start(1, true);
            _mode = 3;
            break;
          case 3:
            _sequence = MorseSequencer(5, _out_pin).dash()->letterBreak()->build();
            _sequence->start(1, true);
            _mode = 4;
            break;
          default:
          case 4:
            digitalWrite(_out_pin, LOW);
            _mode = 0;
            break;
        }
      }
    }
  private:
    unsigned char _out_pin;
    unsigned char _in_pin;
    int _mode = 0;
    unsigned long _last_press = 0;
    MorseSequence* _sequence = NULL;
};

MorseController red(RED_PIN_IN, RED_PIN_OUT);
MorseController green(GREEN_PIN_IN, GREEN_PIN_OUT);

void setup()
{
  Serial.begin(9600);
  
  pinMode(RED_PIN_OUT, OUTPUT);
  pinMode(GREEN_PIN_OUT, OUTPUT);
  pinMode(RED_PIN_IN, INPUT_PULLUP);
  pinMode(GREEN_PIN_IN, INPUT_PULLUP);

  red.start(10, true);
  green.start(10, true);
}

// main loop
void loop()
{
  VariableTimedAction::updateActions();
}
