#include <VariableTimedAction.h>
#include <Servo.h>

// Define Pins
#define SERVO_PIN 10

class JoystickServoController : public VariableTimedAction {
  public:
    Servo servo;
    JoystickServoController()
    {
      servo.attach(SERVO_PIN);
    }
    virtual unsigned long run() override {
      unsigned char pos = analogRead(A0) * (180.0 / 1024.0);
      servo.write(pos);
      return 0;
    }
};

JoystickServoController* servo;

void setup()
{
  servo = new JoystickServoController();
  servo->start(10, true);
}

// main loop
void loop()
{
  VariableTimedAction::updateActions();
}
