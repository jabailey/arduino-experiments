void setup()
{
  pinMode(3, OUTPUT);
  pinMode(5, OUTPUT);
}

// main loop
void loop()
{
  unsigned char pos_x = analogRead(A0) * (180.0 / 1024.0);
  unsigned char pos_y = analogRead(A1) * (180.0 / 1024.0);
  analogWrite(3, pos_x);
  analogWrite(5, pos_y);
}
